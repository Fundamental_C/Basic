/*	
 *	This is a template for a header file in C language
 *	you should use library guards to avoid recursion
 *	A very common style of Header Guard is to use the name of the
 *	header file converting the .h suffix to a _H.
 *
 *	e.g. --->	HTEMPLATE_H	
 */

/*	Preprocessor Directives */
#ifndef HTEMPLATE_H
#define	HTEMPLATE_H
/*	System Header Files	*/

/*	User Header Files */

/*	User Macros	*/

/*	User functions prototypes */
#endif/* HTEMPLATE_H */
/*	The above comment is not necessary, is just for the user reference */
