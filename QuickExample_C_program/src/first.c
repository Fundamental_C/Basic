/* This is an example of a typical C program */

/**
 * 	This line tells the compiler to include the information
 * 	found in the file stdio.h, which is a standard part of all C 
 * 	compiler packages; this file provides support for keyboard 
 * 	input and for displaying output
 */
#include<stdio.h>	/**< Preprocessor instructions*/


/**
 *	Main function entry	
 *	C programs consist of one or more functions, the basic modules of
 *	a C program. This program consists of one function called main. 
 *	The parentheses identify main() as a function name. The int 
 *	indicates that the main() function returns an integer, and 
 *	the void indicates that main() doesn't take any arguments.
 */
int main(void)
{	/**< Beginning of the body of the function	*/
	
	int num; /** a declaration statement */
	num = 1; /** an assignment statement (definition) */
	printf("\nI am a simple and ordinary ");/**< A function call statement */
	printf("computer program\n");
	printf("I am the number %d in this room\n\n",num);
	return 0;/**< a return statement */
}/**< end of the body of the main function */
