#	Basic

This is the repository of the Basic section of the Fundamental_C_for_Embedded_Programmer
here you will find the source code of the examples and the presentations viewed in the lessons,
if you have any comment please send me an email to ing.mtma@gmail.com.
#	Index

##	1.Getting Ready
###	1.1.What is Linux and why use it ?
#### 1.1.1.Bash/Terminal
#### 1.1.2.What is GCC -> Programming Mechanics(compiling, linking)
##	2.Quick Example of a C program
###	2.1.Directives and Header Files
###	2.2.main()function 
###	2.3.Declaration and definition 
### 2.4.Name choice
### 2.5.The puts()	
##	3.Data types
